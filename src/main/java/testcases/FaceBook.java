package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class FaceBook extends SeMethods{
@Test
	public void FB() {
		// TODO Auto-generated method stub
		startApp("chrome", "https://www.facebook.com/");
		WebElement Email = locateElement("id", "email");
		type(Email, "janakidevi1989@gmail.com");
		WebElement pwd = locateElement("id", "pass");
		type(pwd, "Mugunth@02");
		locateElement("xpath", "//input[@value = 'Log In']").click();
		WebElement Search = locateElement("class", "_1frb");
		type(Search,"TestLeaf");
		locateElement("xpath","(//button[@type = 'submit']/i)[1]").click();
		/*WebElement Places = locateElement("xpath","(//div[text()='Places'])[1]");
		Actions builder = new Actions(driver);
		builder.moveToElement(Places).click().build().perform();*/

		WebElement likeText = locateElement("xpath", "(//button[@data-testid='facebar_search_button'])/i");
		System.out.println("Like text" +likeText.getText());
		if(verifyExactText(likeText, "Like")) {
			click(likeText);
			System.out.println("Clicked Successfully");
		} else {
			System.out.println("Already clicked");
		}
		WebElement TFlink = locateElement("xpath" , "(//div[text()='TestLeaf'])[1]");
		click(TFlink);
		System.out.println(verifyTitle("TestLeaf"));
	}
}
